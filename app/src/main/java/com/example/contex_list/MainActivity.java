package com.example.contex_list;

import androidx.appcompat.app.AppCompatActivity;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
int id=-1;
    ListView listSinhVien;
EditText editten,editsdt;
Button btnThem,btnSua,btnXoa;
ArrayList<SinhVien>arrSinhVien;
CustomAdapter adapter;
public static boolean isActionModel=false;
public static List<SinhVien> UserSeletion=new ArrayList<SinhVien>();
public static ActionMode actionMode=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listSinhVien=findViewById(R.id.list_sinhvien);
        listSinhVien.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        listSinhVien.setMultiChoiceModeListener(modeListener);
        anhhh();

        arrSinhVien =new ArrayList<SinhVien>();
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Nguyễn Thị Thu Uyên","1811505310451"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Nguyễn Thị Thu Uyên","1811505310451"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Sếp","016398781"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Mami","016523563"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Dad","022965315"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Honey","99999999"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Bà hàng xóm","036985148"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Mẹ chồng","016398781"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Shipper","016523563"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Tài xế","022965315"));
        arrSinhVien.add(new SinhVien(R.mipmap.ic_launcher_round,"Cô giúp việc","99999999"));

adapter=new CustomAdapter(this,R.layout.item_layout,arrSinhVien);
listSinhVien.setAdapter(adapter);
listSinhVien.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        id= position;
        editten.setText(arrSinhVien.get(position).getTensinhvien());
editsdt.setText(arrSinhVien.get(position).getSdtsv());
    }
});



    }

    AbsListView.MultiChoiceModeListener modeListener= new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater=mode.getMenuInflater();
            inflater.inflate(R.menu.example_menu,menu);
isActionModel=true;
actionMode=mode;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.option1:
                    adapter.removeItems(UserSeletion);
                    mode.finish();
                    return true;

                default:return false;   }


        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
isActionModel=false;
actionMode=null;
UserSeletion.clear();
        }
    };
private void anhhh(){
        listSinhVien=findViewById(R.id.list_sinhvien);
        editten= findViewById(R.id.edit_ten);
        editsdt=findViewById(R.id.edit_sdt);
        btnThem=findViewById(R.id.btn_them);
        btnSua=findViewById(R.id.btn_sua);

        btnThem.setOnClickListener(this);
    btnSua.setOnClickListener(this);

}


    @Override
    public void onClick(View v) {
switch (v.getId()){
    case R.id.btn_them:
        Toast.makeText(this,"click vao button them",Toast.LENGTH_SHORT).show();
String ten=editten.getText().toString();
String sdt= editsdt.getText().toString();
SinhVien temp =new SinhVien(R.mipmap.ic_launcher,ten,sdt);
arrSinhVien.add(temp);
adapter.notifyDataSetChanged();

break;
    case R.id.btn_sua:
       String tendasua=editten.getText().toString();
       String sdtdasua=editsdt.getText().toString();

SinhVien svdasua =new SinhVien(R.mipmap.ic_launcher,tendasua,sdtdasua);
arrSinhVien.set(id,svdasua);
id=-1;
adapter.notifyDataSetChanged();


        break;
}
    }
}