package com.example.contex_list;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapter extends ArrayAdapter {
    Activity activity;
    int layout;
    ArrayList<SinhVien> arrSinhVien;
    List<String> fruiits=new ArrayList<>();

    public CustomAdapter(@NonNull Activity activity, int layout, @NonNull ArrayList<SinhVien> arraySinhvien) {
        super(activity, layout, arraySinhvien);
        this.activity=activity;
        this.layout=layout;
        this.arrSinhVien = arraySinhvien;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater= activity.getLayoutInflater();
//        View row= layoutInflater.inflate(R.layout.item_layout,parent,false);
        convertView=layoutInflater.inflate(layout,null);
        ImageView avatar =(ImageView) convertView.findViewById(R.id.avatar);
        TextView ten=(TextView) convertView.findViewById(R.id.text_ten);
        TextView sdt=(TextView) convertView.findViewById(R.id.text_sdt);

avatar.setImageResource(arrSinhVien.get(position).getAvatar());
ten.setText(arrSinhVien.get(position).getTensinhvien());
        sdt.setText(arrSinhVien.get(position).getSdtsv());
        final CheckBox checkBox =convertView.findViewById(R.id.checkbox);
checkBox.setTag(position);

        if(MainActivity.isActionModel){
            checkBox.setVisibility(View.VISIBLE);
        }else
        {
            checkBox.setVisibility(View.GONE);
        }
checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
           int position=(int)buttonView.getTag();

           if(MainActivity.UserSeletion.contains(arrSinhVien.get(position))){
               MainActivity.UserSeletion.remove(arrSinhVien.get(position));
           }else {
               MainActivity.UserSeletion.add(arrSinhVien.get(position));
           }

MainActivity.actionMode.setTitle(MainActivity.UserSeletion.size()+" items selected .");
    }
});
        return convertView;
    }
    public  void removeItems(List<SinhVien> items){
        for(SinhVien item : items){
            arrSinhVien.remove(item);
        }
        notifyDataSetChanged();
    }
}
