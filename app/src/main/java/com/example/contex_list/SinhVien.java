package com.example.contex_list;

public class SinhVien {
    int avatar ;
    String tensinhvien;
    String sdtsv;
    public SinhVien(){

    }

    public SinhVien(int avatar, String tensinhvien, String sdtsv) {
        this.avatar = avatar;
        this.tensinhvien = tensinhvien;
        this.sdtsv = sdtsv;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getTensinhvien() {
        return tensinhvien;
    }

    public void setTensinhvien(String tensinhvien) {
        this.tensinhvien = tensinhvien;
    }

    public String getSdtsv() {
        return sdtsv;
    }

    public void setSdtsv(String sdtsv) {
        this.sdtsv = sdtsv;
    }
}
